# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_SHG.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_SHG.py $

from guardian import GuardState, GuardStateDecorator
from guardian.state import TimerManager
from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)
from noWaitServo import Servo
import time
import numpy as np

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

#this is to keep emacs python-mode happy from lint errors due to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    notify = None
    log = None

def notify_log(msg):
    notify(msg)
    log(msg)

if sqzconfig.nosqz == True:
    #    nominal = 'IDLE'
    nominal = 'LOCKED'
else:
    nominal = 'LOCKED'

#nominal = 'LOCKED_and_SLOW'
#nominal = 'IDLE'

class SHGParams(EzcaUser):
    CMB_SLOWMON_RAIL_HIGH = 9
    CMB_SLOWMON_RAIL_LOW  = -9
    LOCKING_GAIN = 0
    LOCKED_GAIN = 0

    # counter for lock attempts
    LOCK_ATTEMPT_COUNTER = 0
    LOCK_ATTEMPT_MAX = 10   #5

    FAULT_TIMEOUT_S = 1
    UNLOCK_TIMEOUT_S = 1
    intervention_msg = None

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0

    def in_fault(self):
        fault = False
        warning = ''

        # check the laser output
        if ezca['SQZ-LASER_IR_DC_POWERMON'] < ezca['SQZ-SHG_GRD_BLOCK_IN_TH']:
            fault = True
            fault = False # skip for PMC installation 012224 MN
            warning += 'SQZ laser power low. '

        if ezca['SQZ-SHG_TEC_THERMISTOR_TEMPERATURE'] < 15 or ezca['SQZ-SHG_TEC_THERMISTOR_TEMPERATURE'] > 40:
            fault = True
            warning += 'SHG TEC temperature is out of nominal range. '


        if fault:
            notify(warning)
            
        return fault

    def is_locked(self):        
        #return ezca['SQZ-SHG_TRANS_DC_POWER'] > ezca['SQZ-SHG_GRD_LOCK_TRANS_TH'] and self.CMB_SLOWMON_RAIL_LOW < ezca['SQZ-SHG_SERVO_SLOWMON'] < self.CMB_SLOWMON_RAIL_HIGH
         return ezca['SQZ-SHG_TRANS_DC_POWER'] > 0.5 and self.CMB_SLOWMON_RAIL_LOW < ezca['SQZ-SHG_SERVO_SLOWMON'] < self.CMB_SLOWMON_RAIL_HIGH  #Begum

class SHGShared(SharedState):
    @DecoShared
    def shg(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return SHGParams()

shared = SHGShared()


#############################################
#Functions
def down_state_set():
    ezca['SQZ-SHG_SERVO_IN1EN'] = False
    ezca['SQZ-SHG_SERVO_IN2EN'] = False 
    ezca['SQZ-SHG_SERVO_COMEXCEN'] = False
    ezca['SQZ-SHG_SERVO_FASTEXCEN'] = False     
    ezca['SQZ-SHG_SERVO_COMBOOST'] = 0
    ezca['SQZ-SHG_SERVO_SLOWBOOST'] = 0
    ezca['SQZ-SHG_TEC_LOCKIN_AMPLITUDE'] = 0
    ezca['SQZ-SHG_TEC_LOCKIN_SWITCH'] = 'Off'



#############################################
#Decorator
class lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not shared.shg.is_locked():
            if shared.shg.timer['UNLOCK_TIMEOUT']:
                return 'LOCKLOSS'
        else:
            shared.shg.timer['UNLOCK_TIMEOUT'] = shared.shg.UNLOCK_TIMEOUT_S


class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.shg.in_fault():
            if shared.shg.timer['FAULT_TIMEOUT']:
                return 'FAULT'
        else:
            shared.shg.timer['FAULT_TIMEOUT'] = shared.shg.FAULT_TIMEOUT_S

class power_checker(GuardStateDecorator):
    def pre_exec(self):
        #need to acces this attribute to force the patch
        shared.shg

        if ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] > 140:
            shared.shg.intervention_msg = 'Too much power going into fiber!'
            return 'REQUIRES_INTERVENTION'
        else:
            return


#############################################
#States

class INIT(GuardState):
    request = False

    def run(self):
        return True


#exists only to inform the manager to move into its managed state
class MANAGED(GuardState):
    request = True

    def run(self):
        return True


class IDLE(GuardState):

    def run(self):
        return True


class LOCKLOSS(GuardState):
    """Record lockloss events"""
    request = False
    redirect = False

    def main(self):
        down_state_set()
        # count lock loss
        shared.shg.LOCK_ATTEMPT_COUNTER += 1
        log("LOCK ATTEMPT " + str(shared.shg.LOCK_ATTEMPT_COUNTER))

        if shared.shg.LOCK_ATTEMPT_COUNTER > shared.shg.LOCK_ATTEMPT_MAX:
            shared.shg.LOCK_ATTEMPT_COUNTER = 0
            shared.shg.intervention_msg = "Too Many Lock Failures"
            return 'REQUIRES_INTERVENTION'
        return True


class REQUIRES_INTERVENTION(GuardState):
    request = False
    redirect = False

    def run(self):
        if shared.shg.intervention_msg is not None:
            notify(shared.shg.intervention_msg)
        notify("request non-locking state")
        if ezca['GRD-SQZ_SHG_REQUEST'] in lock_states:
            return False
        else:
            shared.shg.intervention_msg = None
            return True

#-------------------------------------------------------------------------------
# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    index   = 1
    request = False
    goto    = True

    @fault_checker
    def main(self):
        down_state_set()
        return

    @fault_checker
    def run(self):
        return True


class FAULT(GuardState):
    redirect = False
    request = False

    def main(self):
        down_state_set()

    def run(self):
        return not shared.shg.in_fault()

#-------------------------------------------------------------------------------
class LOCKING(GuardState):
    index = 5

    def init_servo(self):
        #servo initialization
        ezca['SQZ-SHG_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-SHG_SERVO_COMBOOST'] = 0
        ezca['SQZ-SHG_SERVO_SLOWBOOST'] = 'Off'
        ezca['SQZ-SHG_SERVO_COMFILTER'] = 'On'
        ezca['SQZ-SHG_SERVO_IN1GAIN'] = shared.shg.LOCKING_GAIN
        
    @fault_checker        
    @power_checker        
    def main(self):
        self.thr = ezca['SQZ-SHG_GRD_LOCK_TRANS_TH'] # threshold
        # self.thr = 0.5 # Begum
        
        # sweep parameters 
        self.sweepMax = 4
        self.sweepMin = 0
        self.sweepStep = 0.03

        self.count = 0
        self.timer['done'] = 0

        self.init_servo()
        self.timeout = 120

        
    @fault_checker        
    @power_checker
    def run(self):
        if not self.timer['done']:
            return

        # Sweep the offset until on resonance then engage servo and boosts            
        if self.count == 0:
            if ezca['SQZ-SHG_TRANS_DC_POWER'] > self.thr:
                log('On resonance, engage lock')
                ezca['SQZ-SHG_SERVO_IN1EN'] = 'On'
                time.sleep(1)
                ezca['SQZ-SHG_SERVO_COMBOOST'] = 2
                time.sleep(1)
                ezca['SQZ-SHG_SERVO_SLOWBOOST'] = 1
                time.sleep(1)
                ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] -= 0.5

                self.count += 1
                self.timer['done'] = 2                
                
            # if the output offset is out of sweep range, go to zero
            elif ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] > self.sweepMax:
                ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] = self.sweepMin
                self.timer['done'] = 2

            else:
                ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] += self.sweepStep

        # Initialize for averaging SHG trans
        elif self.count == 1:
            # gain normalize
            self.timer['avg'] = 2
            self.N = 0
            self.val = 0
            self.count += 1

        # Take average of SHG trans
        elif self.count == 2:
            self.N += 1
            self.val += ezca['SQZ-SHG_TRANS_LF_OUTPUT']
            if self.timer['avg']:
                self.count += 1

        # Set servo gain using SHG trans power to maintain same OLG
        elif self.count == 3:
            val_avg = self.val/self.N
            #ezca['SQZ-SHG_SERVO_IN1GAIN'] = round(-np.log10(val_avg/2960.)*20 + 23) #Begum
            self.count += 1
            self.timer['done'] = 1
            self.timer['lockcheck'] = 1
                
        # If still locked, move on, if not locked, relock
        elif self.count == 4:
            if ezca['SQZ-SHG_GR_DC_POWERMON'] < self.thr:
                log('Lost lock, start again')
                self.init_servo()
                self.count = 0
            elif self.timer['lockcheck']:                
                return True

#-------------------------------------------------------------------------------
class SWEEP(GuardState):
    goto = True

    @fault_checker
    def main(self):
        ezca['SQZ-SHG_SERVO_IN1EN'] = False
        ezca['SQZ-SHG_SERVO_IN2EN'] = False # TODO IN2EN undefined
        ezca['SQZ-SHG_SERVO_COMBOOST'] = 0

        #set the default direction as UP
        self.direction = True
        return

    @fault_checker
    def run(self):
        if ezca['SQZ-SHG_GRD_SWEEP_RATE'] < 0:
            ezca['SQZ-SHG_GRD_SWEEP_RATE'] = -ezca['SQZ-SHG_GRD_SWEEP_RATE']

        if ezca['SQZ-SHG_GRD_SWEEP_LOW'] > ezca['SQZ-SHG_GRD_SWEEP_HIGH']:
            ezca['SQZ-SHG_GRD_SWEEP_LOW'], ezca['SQZ-SHG_GRD_SWEEP_HIGH'] = ezca['SQZ-SHG_GRD_SWEEP_HIGH'], ezca['SQZ-SHG_GRD_SWEEP_LOW']

        if self.direction:
            ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] += ezca['SQZ-SHG_GRD_SWEEP_RATE'] / 16.
            if ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] > ezca['SQZ-SHG_GRD_SWEEP_HIGH']:
                self.direction = False
                ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] = ezca['SQZ-SHG_GRD_SWEEP_HIGH']
        else:
            ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] -= ezca['SQZ-SHG_GRD_SWEEP_RATE'] / 16.
            if ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] < ezca['SQZ-SHG_GRD_SWEEP_LOW']:
                self.direction = True
                ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] = ezca['SQZ-SHG_GRD_SWEEP_LOW']
        return True

#-------------------------------------------------------------------------------
# Tunes SHG temperature to optimize power
class LOCKED_and_SLOW(GuardState):
    index = 50
    
    @power_checker
    @lock_checker
    @fault_checker    
    def main(self):

        # engage excitation
        ezca['SQZ-SHG_TEC_LOCKIN_FREQUENCY'] = 2.7
        ezca['SQZ-SHG_TEC_LOCKIN_AMPLITUDE'] = 0.1
        ezca['SQZ-SHG_TEC_LOCKIN_SWITCH'] = 'On'

        ezca.switch('SQZ-SHG_GR_HP','FM1','ON')
        ezca.switch('SQZ-SHG_GR_DEMOD','FM1','ON')

       
        # create SHG temperature servo
        self.SHG_TEMP_servo = Servo(ezca, 'SQZ-SHG_TEC_SETTEMP', readback='SQZ-SHG_GR_DEMOD_OUTPUT',
                                    gain=-30, ugf=0.1)
        
        self.timer['wait_settle'] = 10
        self.timer['reset'] = 0
        self.reset = False
        self.thr = 0.001
 
        self.fail = False

        
    @power_checker
    @lock_checker
    @fault_checker    


    def run(self):

        if self.reset and self.timer['reset']:
            ezca['SQZ-SHG_GR_DEMOD_RSET'] = 2
            self.reset = False
        
        if self.timer['wait_settle']:
            if not self.fail:
                self.SHG_TEMP_servo.step()

            else:
                notify('TDS might fail. Please check and go to INIT.')
                if (32 < ezca['SQZ-SHG_TEC_SETTEMP'] < 36 and ezca['SQZ-SHG_GR_DC_POWERMON'] > 5):
                    self.fail=False
                    self.SHG_TEMP_servo = Servo(ezca, 'SQZ-SHG_TEC_SETTEMP', readback='SQZ-SHG_GR_DEMOD_OUTPUT',
                                                gain=-30, ugf=0.1)
            
                
            if not (32 < ezca['SQZ-SHG_TEC_SETTEMP'] < 36 and ezca['SQZ-SHG_GR_DC_POWERMON'] > 5):
                ezca['SQZ-SHG_TEC_SETTEMP'] = 34.8
                self.fail = True                

            return not self.fail

#-------------------------------------------------------------------------------
class LOCKED(GuardState):
    index = 75
    
    @lock_checker
    @power_checker
    @fault_checker
    def main(self):
        self.timer['lock_settle'] = 2
        ezca['SQZ-SHG_TEC_LOCKIN_SWITCH'] = 'Off'
        return

    @power_checker
    @lock_checker
    @fault_checker    
    def run(self):
        if self.timer['lock_settle']:
            return True
        
##################################################

edges = [
    ('INIT', 'DOWN'),
    ('FAULT', 'DOWN'),
    ('LOCKLOSS', 'DOWN'),
    ('REQUIRES_INTERVENTION', 'DOWN'),
    
    ('DOWN', 'IDLE'),
    ('IDLE', 'LOCKING'),
    ('LOCKING', 'LOCKED_and_SLOW'),
    ('LOCKED_and_SLOW','LOCKED'),


    ('IDLE', 'SWEEP'),
]

lock_states = [
    'LOCKING', 'LOCKED',
]

